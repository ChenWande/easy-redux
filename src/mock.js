export const getAsyncData = () =>
  new Promise((resolve) => {
    setTimeout(() => {
      resolve({
        name: '张三',
        age: 88,
      });
    }, 2000);
  });
