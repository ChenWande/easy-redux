export const logger = ({ getState, dispatch }) => next => action => {
  console.log('【logger】即将执行:', action)

    // 调用 middleware 链中下一个 middleware 的 dispatch。
  let returnValue = next(action)
  console.log('【logger】执行完成后 state:', getState())
  return returnValue
}

export const thunk = ({ dispatch, getState }) => next => action => {
  if (typeof action === 'function') {
    return action(dispatch, getState);
  }
  return next(action);
};

export const promise = ({ dispatch, getState }) => next => action => {
  if (action.payload instanceof Promise) {
    action.payload.then((payload) => {
      return dispatch({...action, payload})
    })
  }
  return next(action);
};